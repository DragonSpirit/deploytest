const express = require('express');
const app = express();
app.use(express.json());

const db = {
	lights: {
		0: {
			name: 'test',
			history: [
				{ off: +new Date() - 1000 },
				{ on: +new Date() },
			]
		}
	}
};

app.get('/', (req, res) => {
  res.send('Hello World From Deploy!');
});

app.get('/lights', (req, res) => {
	res.json(db.lights);
});

app.get('/light/:id', (req, res) => {
	const {id} = req.params;
	if (id === null || id === undefined) {
		res.sendStatus(400);
		return;
	}
	if (!db.lights[id]) {
		res.sendStatus(404);
		return;
	}
	res.json(db.lights[id]);
});

app.post('/light', (req, res) => {
	const {id, data} = req.body;
	if (id === null || id === undefined) {
		res.sendStatus(400);
		return;
	}
	if (db.lights[id]) {
		res.sendStatus(409);
		return;
	}
	db.lights[id] = data;
	db.lights[id].history = [];
	res.json(db.lights[id]);
});

app.put('/light/:id', (req, res) => {
	const {id} = req.params;
	const {data} = req.body;
	if (id === null || id === undefined) {
		res.sendStatus(400);
		return;
	}
	const newData = {...db.lights[id], ...data};
	db.lights[id] = newData;
	res.json(db.lights[id]);
});

app.post('/light/:id/history', (req, res) => {
	const {id} = req.params;
	const {data} = req.body;
	if (id === null || id === undefined) {
		res.sendStatus(400);
		return;
	}
	db.lights[id].history.push(data);
	res.sendStatus(200);
});

app.delete('/light/:id', (req, res) => {
	const {id} = req.params;
	if (id === null || id === undefined) {
		res.sendStatus(400);
		return;
	}
	if (db.lights[id]) {
		delete db.lights[id];
		res.sendStatus(204);
	} else {
		res.sendStatus(404);
	}
})

app.listen(3000, () => {
  console.log('app listening on port 3000!');
});
