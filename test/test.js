const assert = require('assert');
describe('Array', () => {
    describe('#indexOf()', () => {
        it('should return -1 when the value is not present', () => {
            assert.equal([1, 2, 3].indexOf(4), -1);
        });
    });
    describe('filter', () => {
       it('should correct filter value', () => {
           const mockData = [1, 2, 3, 4];
           const exprectedResult = [2, 4];
           assert.deepEqual(mockData.filter(item => item % 2 === 0), exprectedResult);
       })
    });
});
